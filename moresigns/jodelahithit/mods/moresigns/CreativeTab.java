package jodelahithit.mods.moresigns;

import jodelahithit.mods.firepit.FirePit;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CreativeTab extends CreativeTabs {

	public CreativeTab(int i, String string) {
		super(i, string);
	}

	public ItemStack getIconItemStack() {
		return new ItemStack(FirePit.FirePit);

	}

	public String getTranslatedTabLabel() {
		return "More Signs";
		
	}


	public Item getTabIconItem() {
		return null;
	}
}