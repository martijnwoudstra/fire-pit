package jodelahithit.mods.moresigns;


import jodelahithit.mods.moresigns.blocks.Carpet;
import jodelahithit.mods.moresigns.blocks.Teleport;
import jodelahithit.mods.moresigns.items.ItemSign;
import jodelahithit.mods.moresigns.lib.Ref;
import jodelahithit.mods.moresigns.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntitySign;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;


@Mod(modid = Ref.MODID, name = Ref.MODNAME, version = Ref.VERSION)
public class MoreSigns{

    public static CreativeTabs CT = new CreativeTab(CreativeTabs.getNextID(), "More Signs");

    @SidedProxy(serverSide = Ref.SERVERSIDE, clientSide = Ref.CLIENTSIDE)
    public static CommonProxy proxy;

    public static Block Carpet = new Carpet(Material.ground);
    public static Item Teleport = new Teleport();
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e){

    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e){
        GameRegistry.registerBlock(Carpet, "Carpet");
        GameRegistry.registerItem(Teleport, "Teleporter");
    }
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e){

    }
    
    
    public void LR(Block block, int i, int j, String name) {
        LanguageRegistry.addName(new ItemStack(block, i, j), name);
    }

    public void RB(Block block, String name) {
        GameRegistry.registerBlock(block, block.getUnlocalizedName());
        LanguageRegistry.addName(block, name);
    }

    public void RI(Item item, String name) {
        GameRegistry.registerItem(item, item.getUnlocalizedName());
        LanguageRegistry.addName(item, name);

    }

}