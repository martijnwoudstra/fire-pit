package jodelahithit.mods.moresigns.lib;

public class Ref {

	public static final String MODID = "moresigns";
	public static final String MODNAME = "More Signs";
	public static final String VERSION = "0.1";
	public static final String CLIENTSIDE = "jodelahithit.mods.moresigns.proxy.ClientProxy";
	public static final String SERVERSIDE = "jodelahithit.mods.moresigns.proxy.CommonProxy";
}
