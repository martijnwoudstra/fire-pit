package jodelahithit.mods.af.lib;

import java.io.File;
import java.io.IOException;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

public class lib {
    
    public static void init (File location){
        
        Configuration config = new Configuration(location);

//        File newFile = new File(location + "/Additional_Furnaces.cfg");
        try
        {
            location.createNewFile();
        }
        catch (IOException e)
        {
            System.out.println("Additional Furnaces couldn't find its config! Reason:");
            System.out.println(e);
        }
        

        
        config.load();
        config.addCustomCategoryComment("Blocks","Hallo?");
        Property someProperty = config.get(Configuration.CATEGORY_GENERAL, "SomeConfigString", "nothing");
        someProperty.comment = "This value can be read as a string!";
        
        config.save();
    }
    
    public static int HTCFurnaceIdle;
    public static int HTCFurnaceActive;
}
