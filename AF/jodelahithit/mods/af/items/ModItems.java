package jodelahithit.mods.af.items;

import net.minecraft.item.Item;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
public class ModItems {

	public static Item HafniumDust;
	public static Item HafniumSmallDust;
	public static Item HafniumIngot;
	public static Item TantalumDust;
	public static Item TantalumSmallDust;
	public static Item TantalumIngot;
	public static Item PestleAndMortar;
	public static Item StoneStick;
	public static Item StoneBowl;
	public static Item CoalDust;

	public static void init() {

		HafniumDust = new ItemHafniumDust();
		HafniumSmallDust = new ItemHafniumSmallDust();
		HafniumIngot = new ItemHafniumIngot();
		TantalumDust = new ItemTantalumDust();
		TantalumSmallDust = new ItemTantalumSmallDust();
		TantalumIngot = new ItemTantalumIngot();
		PestleAndMortar = new ItemPestleAndMortar();
		StoneStick = new ItemStoneStick();
		StoneBowl = new ItemStoneBowl();

		RI(HafniumDust, "Hafnium Dust");
		RI(HafniumSmallDust, "Tiny Pile of Hafnium Dust");
		RI(HafniumIngot, "Hafnium Ingot");
		RI(TantalumDust, "Tantalum Dust");
		RI(TantalumSmallDust, "Tiny Pile of Tantalum Dust?");
		RI(TantalumIngot, "Tantalum Ingot");
		RI(PestleAndMortar, "Pestle and Mortar");
		RI(StoneStick, "Stone Stick");
		
	}
	public static void RI(Item item, String name) {
		GameRegistry.registerItem(item, name);
	}

}
