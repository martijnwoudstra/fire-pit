//package jodelahithit.mods.af.block;
//
//import java.util.Random;
//
//import javax.swing.Icon;
//
//import jodelahithit.mods.af.AF;
//import net.minecraft.block.Block;
//import net.minecraft.block.BlockContainer;
//import net.minecraft.block.material.Material;
//import net.minecraft.client.renderer.texture.IIconRegister;
//import net.minecraft.entity.EntityLivingBase;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.item.ItemStack;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.tileentity.TileEntityFurnace;
//import net.minecraft.util.IIcon;
//import net.minecraft.util.MathHelper;
//import net.minecraft.world.World;
//import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
//import cpw.mods.fml.relauncher.Side;
//import cpw.mods.fml.relauncher.SideOnly;
//
//public class BlockHTCFurnace extends BlockContainer {
//    
//    public boolean isActive;
//    
//    @SideOnly(Side.CLIENT)
//    
//    private Icon iconTop;
//    private Icon iconFront;
//    private Icon iconMiddle;
//
//    public BlockHTCFurnace(boolean isActive) {
//        super(Material.iron);
//
//        this.isActive = isActive;
//        setLightValue(0.8F);
//        setCreativeTab(AF.CT);
//        setHardness(5F);
//        setStepSound(soundStoneFootstep);
//
//    }
//    
//    @SideOnly(Side.CLIENT)
//    public void registerIcons(IIconRegister iconregister) {
//        this.iconTop = iconregister.registerIcon(AF.modid + ":" + "models/" + "AF_FurnaceTop");
//        this.iconFront = iconregister.registerIcon(AF.modid + ":" + "models/" + (this.isActive ? "AF_FurnaceFrontActive" : "AF_FurnaceFrontIdle"));
//        this.iconMiddle = iconregister.registerIcon(AF.modid + ":" + "models/" + "AF_FurnaceSide");
//    }
//    
//    @SideOnly(Side.CLIENT)
//    public IIcon getIcon(int side, int metadata){
//        return (IIcon) (side == 1 ? this.iconTop : (side == 0 ? this.iconMiddle : (side != metadata ? this.iconMiddle : this.iconFront)));
//    }
//
//    public Block getBlockDropped(int metadata, Random random, int fortune) {
//        return ModBlocks.HTCFurnaceIdle;
//    }
//
//    @Override
//    public void onBlockAdded(World world, int x, int y, int z) {
//        super.onBlockAdded(world, x, y, z);
//        this.setDefaultDirection(world, x, y, z);
//    }
//
//    private void setDefaultDirection(World world, int x, int y, int z) {
//        if (!world.isRemote) {
//            int l = world.getBlockId(x, y, z - 1);
//            int il = world.getBlockId(x, y, z + 1);
//            int jl = world.getBlockId(x - 1, y, z);
//            int kl = world.getBlockId(x + 1, y, z);
//            byte b0 = 3;
//
//            if (Block.opaqueCubeLookup[l] && !Block.opaqueCubeLookup[il]) {
//                b0 = 3;
//            }
//
//            if (Block.opaqueCubeLookup[il] && !Block.opaqueCubeLookup[l]) {
//                b0 = 2;
//            }
//
//            if (Block.opaqueCubeLookup[kl] && !Block.opaqueCubeLookup[jl]) {
//                b0 = 5;
//            }
//
//            if (Block.opaqueCubeLookup[jl] && !Block.opaqueCubeLookup[kl]) {
//                b0 = 3;
//            }
//            world.setBlockMetadataWithNotify(x, y, z, b0, 2);
//        }
//    }
//    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ){
//        if (!world.isRemote){
//            FMLNetworkHandler.openGui(player, AF.instance, AF.instance.guiIdHTCFurnace, world, x, y, z);
//        }
//        return true;
//    }
//    @Override
//    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entityLivingBase, ItemStack itemStack) {
//        int l = MathHelper.floor_double(entityLivingBase.rotationYaw * 4.0F / 360.0F + 0.5D) & 3;
//        if (l == 0) {
//            world.setBlockMetadataWithNotify(x, y, z, 2, 2);
//        }
//        if (l == 1) {
//            world.setBlockMetadataWithNotify(x, y, z, 5, 2);
//        }
//        if (l == 2) {
//            world.setBlockMetadataWithNotify(x, y, z, 3, 2);
//        }
//        if (l == 3) {
//            world.setBlockMetadataWithNotify(x, y, z, 4, 2);
//        }
//        if (itemStack.hasDisplayName()) {
//            ((TileEntityFurnace) world.getBlockTileEntity(x, y, z)).setGuiDisplayName(itemStack.getDisplayName());
//        }
//    }
//
//    @Override
//    public TileEntity createNewTileEntity(World world) {
//        return new TileEntityFurnace();
//    }
//
//	@Override
//	public TileEntity createNewTileEntity(World var1, int var2) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//}
