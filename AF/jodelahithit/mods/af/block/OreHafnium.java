package jodelahithit.mods.af.block;

import java.util.Random;

import jodelahithit.mods.af.AF;
import jodelahithit.mods.af.items.ModItems;
import jodelahithit.mods.af.lib.ModInfo;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class OreHafnium extends Block {
    public OreHafnium() {
        super(Material.rock);
        this.setHardness(1f);
        this.setStepSound(soundTypeStone);
        this.setHarvestLevel("pickaxe", 2);
        this.setBlockName("Hafnium Ore");
        this.setBlockTextureName(ModInfo.MODID + ":" + "ores/AF_OreHafnium");

    }

    public Item getItemDropped(int metadata, Random random, int fortune) {
        return ModItems.HafniumSmallDust;
    }

    public int quantityDropped(Random par1Random) {
        return par1Random.nextInt(2) + 1;
    }

    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconregister) {
        this.blockIcon = iconregister.registerIcon(AF.modid + ":" + "ores/" + this.getUnlocalizedName().substring(5));
    }

}
