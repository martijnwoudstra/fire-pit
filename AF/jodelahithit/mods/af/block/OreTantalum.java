package jodelahithit.mods.af.block;

import java.util.Random;

import jodelahithit.mods.af.AF;
import jodelahithit.mods.af.items.ModItems;
import jodelahithit.mods.af.lib.IAF;
import jodelahithit.mods.af.lib.ModInfo;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class OreTantalum extends Block implements IAF {
	public OreTantalum() {
		super(Material.rock);

		this.setHardness(1f);
		this.setStepSound(soundTypeStone);
        this.setHarvestLevel("pickaxe", 2);
        this.setBlockName("Tantalum ore");
	}
    public Item getItemDropped(int metadata, Random random, int fortune) {
        return ModItems.TantalumSmallDust;
    }
			
	public int quantityDropped(Random par1Random)
    {
        return par1Random.nextInt(2) + 1;
    }
	@Override
	public String getName() {
		return null;
	}
	@Override
	public String TextureLocation() {
		return "ores/";
	}



}
