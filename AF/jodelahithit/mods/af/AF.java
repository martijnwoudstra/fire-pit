package jodelahithit.mods.af;

import java.io.File;

import jodelahithit.mods.af.block.ModBlocks;
//import jodelahithit.mods.af.gui.GuiHandler;
import jodelahithit.mods.af.items.ModItems;
import jodelahithit.mods.af.lib.ModInfo;
import jodelahithit.mods.af.lib.lib;
import jodelahithit.mods.af.misc.AFCreativeTab;
import jodelahithit.mods.af.misc.GenerationHafnium;
import jodelahithit.mods.af.misc.GenerationTantalum;
import jodelahithit.mods.af.proxy.AFCommonProxy;
import jodelahithit.mods.af.tileEntity.TileEntityHTCFurnace;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = ModInfo.MODID, name = ModInfo.NAME, version = ModInfo.VERSION)
public class AF {
    public static final String modid = "AdditionalFurnaces";
    public static CreativeTabs CT = new AFCreativeTab(CreativeTabs.getNextID(), "Additional Furnaces");

    @Instance(modid)
    public static AF instance;

    @SidedProxy(clientSide = ModInfo.CLIENTSIDE, serverSide = ModInfo.SERVERSIDE)
    public static AFCommonProxy proxy;


    public static final int guiIdHTCFurnace = 0;

    @EventHandler
    public void Preinit(FMLPreInitializationEvent event) {
        lib.init(event.getSuggestedConfigurationFile());
    }

    @EventHandler
    public void load(FMLInitializationEvent event) {

    	ModBlocks.init();
    	ModItems.init();

        // OREDICTIONARY

        OreDictionary.registerOre("blockStone", Blocks.stone);
        OreDictionary.registerOre("stickStone", ModItems.StoneStick);

        // RECIPES
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(ModItems.StoneStick, new String[] { "X", "X" }, 'X', "blockStone"));

        GameRegistry.addShapelessRecipe(new ItemStack(ModItems.HafniumDust), new Object[] { new ItemStack(ModItems.HafniumSmallDust), new ItemStack(ModItems.HafniumSmallDust), new ItemStack(ModItems.HafniumSmallDust), new ItemStack(ModItems.HafniumSmallDust) });
        GameRegistry.addShapelessRecipe(new ItemStack(ModItems.TantalumDust), new Object[] { new ItemStack(ModItems.TantalumSmallDust), new ItemStack(ModItems.TantalumSmallDust), new ItemStack(ModItems.TantalumSmallDust), new ItemStack(ModItems.PestleAndMortar, 1, 32767) });
        GameRegistry.addSmelting(ModItems.HafniumDust, new ItemStack(ModItems.HafniumIngot, 3), 50F);
        GameRegistry.addSmelting(ModItems.PestleAndMortar, new ItemStack(Blocks.dirt, 10), 50);

        // GENERATORS
        GameRegistry.registerWorldGenerator(new GenerationTantalum(), 0);
        GameRegistry.registerWorldGenerator(new GenerationHafnium(), 0);

        // PROXY
        proxy.registerRenderThings();
        proxy.LoadRenderers();

        // TILEENTITYS

        GameRegistry.registerTileEntity(TileEntityHTCFurnace.class, "AF_HTCFurnace");

        // MISC

//        GuiHandler guiHandler = new GuiHandler();
    }

    public void LR(Block block, int i, int j, String name) {
        LanguageRegistry.addName(new ItemStack(block, i, j), name);
    }

    public void RB(Block block, String name) {
        GameRegistry.registerBlock(block, block.getUnlocalizedName());
        LanguageRegistry.addName(block, name);
    }

    public void RI(Item item, String name) {
        GameRegistry.registerItem(item, item.getUnlocalizedName());
        LanguageRegistry.addName(item, name);

    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }

}